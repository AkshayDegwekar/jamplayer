package iitm.apl.algorithm;

/**
 * This class implements the Levenshtein distance for two strings. This is a
 * second optimized version where only O(m) memory and O(mn) time are uesd.
 * Where m,n re the lengths of the two strings
 * 
 * @author Akshay
 * 
 */
public class Levenshtein implements IMetric<String> {

	@Override
	public int distance(String source, String destination) {
		int[] currentRow = new int[destination.length() + 1];
		int[] previousRow = new int[destination.length() + 1];

		for (int j = 0; j <= destination.length(); j++)
			previousRow[j] = j;

		for (int i = 1; i <= source.length(); i++) {
			for (int j = 1; j <= destination.length(); j++) {
				currentRow[0] = i;
				if (source.charAt(i - 1) == destination.charAt(j - 1))
					currentRow[j] = previousRow[j - 1];
				else
					currentRow[j] = min((previousRow[j] + 1), // Deletion
							(currentRow[j - 1] + 1), // Instertion
							(previousRow[j - 1] + 1) // Substitution
					);
			}
			previousRow = currentRow;
		}
		return currentRow[destination.length()];
	}

	private int min(int a, int b, int c) {
		int temp = (a < b) ? a : b;
		return (temp < c) ? temp : c;
	}
}
