package iitm.apl.algorithm;

public interface IMetric<T> {
	public int distance(T source, T destination);
}
